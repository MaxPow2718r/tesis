# Tesis

Este repositorio contiene los documentos de mi tesis. En las figuras se pueden
ver varias imágenes que se descartaron en el documento final y algunas ideas
quedaron comentadas entre el Latex.

Todos los cálculos fueron hechos en Python a menos que se indique lo contrario,
los códigos se puede encontrare en mi otro repositorio para este trabajo en
[Gitlab](https://gitlab.com/MaxPow2718r/calculos_tesis), pero el README de ese
repositorio puede estar un poco desactualizado.

## Compilar este documento

Para compilar yo utilicé `latexmk` pero se puede hacer con `pdflatex`, en
cualquier caso se necesita tener instalado [`inkscape`](https://inkscape.org/)
para que pueda compilar.

Si se usa `pdflatex` se debe correr:

```console
pdflatex -shell-escape main.tex
```

Si se usa `latexmk`, ya que este repositorio contiene un archivo de
configuración `.latexmkrc`, bastará con ejecutar:

```console
latexmk
```

## Descargar el documento

Si se quiere descargar una vesión ya compilada revise la pestaña de
[_Releases_](https://gitlab.com/MaxPow2718r/tesis/-/releases) de este
repositorio y descargue la última versión en PDF.
