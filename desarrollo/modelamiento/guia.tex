% Modelamiento del sistema sin el polímero
% Presentar ecuaciones que describen como se calculan los parámetros de la guia.

\subsection{Ecuaciones del modelo de guía de ondas}
\label{sub_sec:ecuaciones_del_modelo_de_guía_de_ondas}

Como ya se ha planteado, el sistema está modelado por una guía de ondas
\textit{slot line}, utilizando la teoría descrita en la sección
\ref{sub_sec:lineas_de_transmisión} se pueden derivar ecuaciones que
caractericen su impedancia, la cual será la base del sistema sensor.

De las ecuaciones \ref{eq:tel_voltaje} y \ref{eq:tel_corriente} se puede
multiplicar ambas por $\frac{\mathrm{d}}{\mathrm{d}z}$ y obtener dos ecuaciones
diferenciales separadas.

\begin{align}
	\frac{\mathrm{d}^2 V(z)}{\mathrm{d}z^2} &=
	(R + \jmath \omega L)(G + \jmath \omega C) V(z) \\
	\frac{\mathrm{d}^2 I(z)}{\mathrm{d}z^2} &=
	(G + \jmath \omega C)(R + \jmath \omega L) I(z)
\end{align}
lo cual se puede reescribir como:
\begin{align}
	\frac{\mathrm{d}^2 V(z)}{\mathrm{d}z^2} - \gamma^2 V(z) &= 0 \\
	\frac{\mathrm{d}^2 I(z)}{\mathrm{d}z^2} - \gamma^2 I(z) &= 0
\end{align}
donde
\begin{equation}
\gamma = \alpha + \jmath \beta = \sqrt{(R + \jmath \omega L)(G + \jmath \omega C)}
\end{equation}
es la constate de propagación. Las ecuaciones de desplazamiento se pueden
encontrar como:
\begin{align}
	V(z) &= V_o^+ e^{-\gamma z} + V_o^- e^{\gamma z} \\
	I(z) &= I_o^+ e^{-\gamma z} + I_o^- e^{\gamma z}
\end{align}
aplicando la ecuación \ref{eq:tel_voltaje} en la ecuación del voltaje anterior
se tiene que:
\begin{equation}
I(z) = \frac{\gamma}{R + \jmath \omega L}
\left( V_o^+ e^{-\gamma z} - V_o^- e^{\gamma z} \right)
\end{equation}

Por ley de Ohm se puede obtener que la impedancia característica es:
\begin{equation}\label{eq:imp_caracteristica}
	Z_0 = \frac{R + \jmath \omega L}{\gamma}
	= \sqrt{\frac{R + \jmath \omega L}{G + \jmath \omega C}}
\end{equation}

En la sección \ref{sub_sec:slot_line} se mostró que la capacitancia de una
\textit{slot line}  arbitraria está dada por la ecuación \ref{eq:slot_cap}.
Por lo tanto, ya se tiene uno de los parámetros que definen la impedancia
característica de la guía.

\subsubsection{Capacitancia}
\label{subsub_sec:capacitancia}

Utilizando los valores asociados a la figura \ref{fig:dimensiones} se puede
expresar la capacitancia como:

\begin{equation}
	C = \epsilon_o \epsilon_{eff_1}
	\frac{K\left[ \sqrt{1 - \left(\frac{a}{2w + a}\right)^2} \right]}
	{K\left[ \frac{a}{2w + a} \right] }
\end{equation}
donde:
\begin{equation}
	\epsilon_{eff_1} = 1 + (\epsilon_{sus} - 1)q
\end{equation}
\begin{equation}
	q = \frac{1}{2}
	\frac{K\left[\sqrt{1 - \left(\frac{\tanh\left(\frac{\pi a/2}{2s}\right) }
	{\tanh \left( \frac{\pi(w + a/2)}{2s} \right) } \right)^2 }\right]}
	{K\left[\frac{\tanh\left(\frac{\pi a/2}{2s}\right) }
	{\tanh \left( \frac{\pi(w + a/2)}{2s} \right) } \right]}
	\frac{K\left[ \sqrt{1 - \left(\frac{a}{2w + a}\right)^2} \right]}
	{K\left[ \frac{a}{2w + a} \right] }
\end{equation}
siendo $K[*]$ la integral elíptica de primer orden como se vio en al sección
\ref{sub_sec:slot_line}.

\subsubsection{Resistencia}
\label{subsub_sec:resistencia}

La resistencia puede ser calculada como la suma de las resistencias en corriente
continua y en alterna. Las resistencias en continua se pueden calcular con la
ecuación de electrostática \cite{ulaby2001fundamentals}.
\begin{equation}\label{eq:resistencia_dc}
R = \frac{l}{\sigma A}
\end{equation}
donde $l$ es el largo de la línea, en este caso es coincidente con el valor $l$
de la \textit{slot line} , $\sigma$ es la conductividad del material y $A$ es el área
de la sección transversal, por lo que la resistencia en corriente continua es:
\begin{equation}
	R_{DC} = \frac{l}{\sigma w h}
\end{equation}

La resistencia en corriente alterna se puede calcular utilizando la misma
ecuación, no obstante, su área se ve reducida por la frecuencia debido al efecto
pelicular. Por lo tanto queda:
\begin{equation}\label{eq:resistencia_ac}
	R_{AC} = \frac{l}{\sigma w \delta}
\end{equation}
donde:
\begin{equation}
	\delta = \sqrt{\frac{2}{\omega \mu \sigma}}
\end{equation}
en este caso $\mu$ es la permeabilidad magnética del material.

Para la resistencia del sistema completo, es necesario considerar las
resistencias de ambas lineas, las cuales se asumen ser iguales, por lo que la
resistencia de la \textit{slot line} es:
\begin{equation}
	R_{\mathrm{total}} = 2 (R_{DC} + R_{AC})
\end{equation}

\subsubsection{Inductancia}
\label{subsub_sec:inductancia}

La inductancia, la capacitancia y la impedancia del sistema están relacionadas
por \cite{orfanidis2002electromagnetic}:
\begin{equation}
L = \mu \frac{Z}{\eta}; \qquad C = \epsilon \frac{\eta}{Z}
\end{equation}
donde $\eta = \sqrt{\mu/\epsilon}$ es la impedancia del medio. De esto se puede
extraer que:
\begin{equation}
L = \mu_o \epsilon_o \frac{1}{C}
\end{equation}

\subsubsection{Conductancia}
\label{subsub_sec:conductancia}

De \cite{gevorgian2001line} se tiene que la conductancia de una \textit{slot
line} está dada por:

\begin{equation}
	G = \frac{\sqrt{\epsilon_{eff_1}}}{120 \pi}
	\frac{K\left[ \frac{a}{a + w} \right]}
	{K\left[\left( 1 - \left( \frac{a}{a + w} \right)^2 \right) \right] }
\end{equation}
siendo $G$ la conductancia del sustrato, el resto de variables son las mismas
definidas en las ecuaciones de la capacitancia.

\subsection{Impedancia del sistema}
\label{sub_sec:impedancia_del_sistema}

Dada la impedancia característica de la ecuación \ref{eq:imp_caracteristica} se
puede obtener la impedancia total de una guia de ondas con carga como:

\begin{equation}\label{eq:imp_total}
Z = Z_0 \frac{Z_L + Z_0 \tanh(\gamma l)}{Z_0 + Z_L \tanh(\gamma l)}
\end{equation}
donde $Z_L$ es la impedancia de la carga, para un sistema libre de carga se puede
asumir que la carga tiende a infinito, por lo que cuando $Z_L \to \infty$:
\begin{equation}\label{eq:imp_sin_carga}
Z = \frac{Z_0}{\tanh(\gamma l)}
\end{equation}

Dada la ecuación \ref{eq:imp_total} se pueden obtener dos impedancias, una
cuando el sistema no tiene un polímero, dada por los parámetros modelados
previamente, siendo estos la capacitancia, la resistencia, la inductancia y la
conductancia de la guía de ondas; por otro lado, cuando se agregué un polímero
al sistema, la impedancia cambiara. Este cambio de impedancia se puede
entender como el polímero añadiendo otra conductancia y capacitancia al sistema.
Esta admitancia en paralelo es la que se puede ver en la figura
\ref{fig:circuito_equivalente} representada por el paralelo entre $G_p$ y $C_p$.

\begin{figure}
\centering
\begin{circuitikz}[american voltages]
\draw (0, 0) node[ocirc] (A) {} node[above] {$-$};
\draw (0, 4) node[ocirc] (B) {} node[below] {$+$};
% \draw[-latex] (0, 2.5) -- node[below] {$i(z{,} t)$} (2, 2.5);
\draw (B) to [R=$R$] ++(2, 0)
to [L=$L$] ++(2, 0)
node[circ] (f1) {}
to [short] ++(0, -1)
node[circ] (C) {};
\draw (C) to [short] ++(-1, 0)
to [R=$G$] ++(0, -2)
to [short] ++(1, 0)
node[circ] (D) {}
to [short] ++(0, -1)
node[circ] (f2) {}
to [short] (A)
to [open=$v(z{,} t)$] (B);
\draw (C) to[short] ++(1, 0)
to [C=$C$] ++(0, -2)
to [short] (D);
\draw (f1) to [short] ++(4, 0)
node[circ] (f3) {}
to [short] ++(0, -1)
node[circ] (E) {};
\draw (E) to [short] ++(-1, 0)
to [R=$G_p$] ++(0, -2)
to [short] ++(1, 0)
node[circ] (F) {}
to [short] ++(0, -1)
node[circ] (f4) {}
to [short] (f2);
\draw (E) to[short] ++(1, 0)
to [C=$C_p$] ++(0, -2)
to [short] (F);
\draw (f3) to [short] ++(3, 0)
node[ocirc] (fin1) {} node[below] {$+$};
\draw (f4) to [short] ++(3, 0)
node[ocirc] (fin2) {} node[above] {$-$};
\draw (fin1) to [open=$v(z + \Delta z {,} t)$] (fin2);
% \draw (11, 0) node[ocirc] (D) {} node[above] {$-$};
% \draw (fin2) to [open=$v \Delta z {,} t)$] (fin1);
\end{circuitikz}
\caption{Circuito equivalente del sistema con un polímero
\cite{fuhrhop2016electrical}.}
\label{fig:circuito_equivalente}
\end{figure}

Si denotamos la admitancia del sistema como $Y$ se puede expresar esta como:
\begin{equation}\label{eq:adm_sensor}
Y = G_t + \jmath \omega C_t; \quad
C_t = C + C_p; \quad
G_t = G + G_p
\end{equation}
donde $G_t$ y $C_t$ son la conductancia total y la capacitancia total,
respectivamente, cuando se agrega una película de polímero cuya conductancia y
capacitancia están denotadas por $G_p$ y $C_p$.


% La carga del sistema, será eventualmente, el polímero sensor. La ecuación
% \ref{eq:imp_sin_carga} nos permite calcular la impedancia del la guía de ondas
% sin el polímero, luego esta puede ser comprada con el con la impedancia total
% del sistema con carga de la ecuación \ref{eq:imp_total} para corroborar como la
% aplicación de un polímero afectará la resistencia del sistema. Finalmente,
% cuando se comprenda la relación entre la impedancia de carga y una concentración
% específica de gas se podrá modelar, con estas ecuaciones, el comportamiento como
% sensor del sistema.
