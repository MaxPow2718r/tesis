% - Simulación del sistema con diferentes materiales
%   - Análisis del uso de diferentes materiales

\subsection{Simulaciones de la guía de ondas con diferentes materiales}
\label{sub_sec:simulaciones_de_la_guía_de_ondas_con_diferentes_materiales}

Utilizando el software multifísico COMSOL \cite{comsol} se modela el sistema
propuesto en 3D (en el apéndice \ref{chap:simulaciones_en_comsol} se explica con
más detalle como se desarrollaron estas simulaciones), a la guía de ondas
planteada en la figura \ref{fig:dimensiones}, además se añade una cámara en la
cual se calcularán los parámetros de la guía, dicha cámara está llena de aire.
La figura \ref{fig:camara} representa el sistema descrito en el software, ademas
en la figura \ref{fig:sensores} se muestran los modelos del sensor en 3D, este
está representado con los mismos colores que en la figura \ref{fig:dimensiones}
para facilitar comprender que partes presentan qué dimensión. Finalmente, las
dimensiones de la cámara de gas, la cual se modela con un cilindro, están dadas
por la tabla \ref{table:camara_dim}.

\begin{figure}
\centering
\includegraphics[width=0.8\textwidth]{images/chamber_wire_frame.png}
\caption{Representación del sensor en una cámara de gas en su modelo de alambres.}
\label{fig:camara}
\end{figure}

\begin{figure}
\centering
\begin{subfigure}{0.4\textwidth}
\centering
\includegraphics[width=\textwidth]{images/chamber_sensor_no_film.png}
\caption{Modelo sin película.}
\label{fig:sensor_comsol_no_film}
\end{subfigure}
\hfill
\begin{subfigure}{0.4\textwidth}
\centering
\includegraphics[width=\textwidth]{images/chamber_sensor.png}
\caption{Modelo con película.}
\label{fig:sensor_comsol}
\end{subfigure}
\caption{Modelos utilizados y diseñados en COMSOL para las simulaciones.}
\label{fig:sensores}
\end{figure}

\begin{table}
	\centering
	\caption{Dimensiones de la cámara de gas.}
	\label{table:camara_dim}
	\begin{tabular}{c r l}
		\hline
		Valor & Valor evaluado $(\si{\micro\metre})$ & Dimensión \\
		\hline
		\hline
		$l$   & $6000$                               & Radio \\
		$2s$  & $3000$                               & Alto \\
		\hline
	\end{tabular}
\end{table}

Primero se simulará el sistema sin la película de polímero para ver la respuesta
en frecuencia de la \textit{slot line} sola. Esto utilizando el modelo de la
figura \ref{fig:sensor_comsol_no_film}. Para todas las simulaciones se utilizará
el rango de frecuencia entre los $50 \si{\hertz}$ y los $2 \si{\mega\hertz}$,
para refinar los resultados en este rango, en el software se computan como 3
rangos con 100 puntos cada uno, pero que en su total son el rango antes
mencionado, estos son: \begin{enumerate} \item $50 \si{\hertz}$ a $50
\si{\kilo\hertz}$, \item $50 \si{\kilo\hertz}$ a $500 \si{\kilo\hertz}$ y \item
$500 \si{\kilo\hertz}$ a $2 \si{\mega\hertz}$.  \end{enumerate} Al simular el
caso previamente descrito se obtienen, del programa de simulación, la impedancia
característica del sistema, estos datos, para el rango completo de frecuencias
son extraídos como tablas (archivos .txt con los datos tabulados) y
posteriormente analizados y graficados utilizando Python. La figura
\ref{fig:sim_slot_line} muestra los gráficos obtenidos de la simulación. En esta
gráfica se ve, en escala logarítmica para el eje vertical, la impedancia
característica de la guía de ondas con respecto a la frecuencia. Es posible
notar que no existe una mayor diferencia al cambiar de material conductor al
sistema, mas existe un cambio notable (en las curvas) con respecto a los cambios
en el material del sustrato.  Al comparar la impedancia entre los conductores se
ve, por ejemplo, que entre el cobre y el oro a los $220 \si{\kilo\hertz}$ hay
una diferencia de tan solo $13 \si{\nano\ohm}$. La impedancia cae rápidamente al
aumentar la frecuencia, tanto así que en los primeros $50 \si{\kilo\hertz}$ el
sistema ha disminuido su impedancia en un $99 \%$.

\begin{figure}
\centering
% \includegraphics[width=\textwidth]{images/chamber_all_comb_no_film_in_air.png}
\input{images/chamber_all_comb_no_film_in_air.pgf}
\caption[Respuesta de las diferentes combinaciones sin polímero]{Respuesta del
\textit{slot line} al rango completo de frecuencias. En este caso se toman todas
las 12 posibles combinaciones que es posible crear con los materiales propuestos
para la guía de ondas. En el eje $x$, para todos los gráficos, se encuentra la
frecuencia y en el eje $y$, igualmente para los 4 gráficos, se encuentra el
módulo de la impedancia característica en escala logarítmica. Cada gráfico
muestra las diferentes combinaciones de los sustratos y cada uno representa el
uso de un material conductor en particular. Este gráfico fue hecho en Python con
los datos obtenidos de COMSOL.}
\label{fig:sim_slot_line}
\end{figure}

Dado que al cambiar los conductores no hay un gran cambio en el sistema y que, a
pesar de existir un cambio en la impedancia al cambiar el sustrato, no hay un
cambio de comportamiento en la impedancia cuando se utilizan diferentes
materiales de sustrato, se tomará una sola combinación en adelante para realizar
los análisis. Si se considera un sistema armado con cobre como
material conductor y FP4 \cite{fp4} como sustrato al hacer el mismo cómputo con
el modelo
analítico y compararlo con su contraparte de COMSOL, se obtiene el gráfico de la
figura \ref{fig:cobre_analitico}. Como se previó, ambos sistemas no computan en
mismo resultado, pero los comportamientos de sus curvas son iguales. No
obstante, la diferencia entre los valores resulta ser aguda, por ejemplo, a la
frecuencia de $2 \si{\mega\hertz}$ el valor analítico varía en un $182 \%$ con
respecto al valor calculado con el software de simulación. Este valor de
diferencia se mantiene prácticamente constante en todo el rango de frecuencia
variando tan solo un $0.000007 \%$. Por otra parte, es considerable los tiempos
de cómputo de cada uno de los métodos, al calcular los valores en el espectro
completo de frecuencia para todas las combinaciones con el software de
simulación toma $10$ minutos y $12$ segundos, mientras que el valor analítico
computado en Python toma tan solo $1.27$ segundos. Estos valores pueden variar
dependiendo de la hardware disponible. Se propone entonces que el modelo
analítico puede ayudar a dar una idea general del comportamiento del sistema mas
no una buena aproximación, y se recomienda utilizar el software de simulación
para cálculos que requieran de mejor exactitud.

% tiempo de computo en python 1.27 segundos

\begin{figure}[t]
\centering
% \includegraphics[width=0.8\textwidth]{images/computos_cobre_fp4_sin_polimero.png}
% \includesvg[width=0.8\textwidth]{images/computos_cobre_fp4_sin_polimero.svg}
\scalebox{0.8}{\input{images/computos_cobre_fp4_sin_polimero.pgf}}
\caption{Comparación entre los valores calculados de manera analítica con Python
y aquellos simulados en COMSOL.}
\label{fig:cobre_analitico}
\end{figure}

Al agregar la película de polímero al sistema y utilizar el modelo de la figura
\ref{fig:sensor_comsol}, se pueden calcular las mismas combinaciones de
materiales agregando las variaciones de materiales para cada película de
polímero. Para estas simulaciones se utilizan los mismos parámetros mostrados en
tablas anteriores, las dimensiones de la película de polímero están dadas por la
tabla \ref{table:dim_poly}. Las figuras \ref{fig:resp_ppy},
\ref{fig:resp_pedot} y \ref{fig:resp_pani} representan las respuestas del
sistema en aire utilizando una película de PPy, PEDOT y PANI, respectivamente,
cuando el material de los conductores es cobre.

\begin{table}
	\centering
	\caption{Dimensiones de la película de polímero.}
	\label{table:dim_poly}
	\begin{tabular}{ll}
		\hline
		Dimensión $(\si{\micro\metre})$ & Descripción           \\
		\hline
		\hline
		2036                            & Ancho de la película  \\
		6000                            & Largo de la película  \\
		1                               & Grosor de la película \\
		\hline
	\end{tabular}
\end{table}

\begin{figure}
\centering
% \includegraphics[width=0.8\textwidth]{images/chamber_PPy_in_air.png}
\input{images/chamber_PPy_in_air-Cobre.pgf}
\caption[Impedancia del sensor con PPy en aire]{Respuesta del sistema en la
cámara de gas utilizando una película de polímero de PPy cuando la cámara está
llena de aire. En este caso se utilizaron las combinaciones posibles con los
diferentes materiales de sustrato. Se considera que las líneas conductoras son
de cobre. En el apéndice \ref{chap:figuras_extras}, las figuras \ref{fig:resp_ppy_alum},
\ref{fig:resp_ppy_est} y \ref{fig:resp_ppy_oro} muestran cómo es la misma
situación cuando se utilizan otros materiales en las líneas conductoras. Al
tomar estos cuatro gráficos en conjunto, se puede notar que no existe gran
diferencia al cambiar el material conductor en la \textit{slot line} salvo leves
cambios producidos en las frecuencias más altas con el óxido de silicio.}
\label{fig:resp_ppy}
\end{figure}

\begin{figure}
\centering
% \includegraphics[width=\textwidth]{images/chamber_PEDOT_in_air.png}
\input{images/chamber_PEDOT_in_air-Cobre.pgf}
\caption[Impedancia del sensor con PEDOT en aire]{Respuesta del sistema
utilizando una película de PEDOT:PSS cuando la cámara de gas está llena de aire.
Al igual que en el gráfico para el sistema con PPy se utilizan las variaciones
cuando se cambia el material del sustrato. En el eje $x$ se presenta la frecuencia
mientras que en el eje $y$ se presenta la impedancia del sistema. Se puede ver
que este se comporta similar al PPy, teniendo una gran variación en bajas
frecuencias, pero estabilizándose a medida que la frecuencia aumenta, no
obstante, su impedancia es menor. En el apéndice \ref{chap:figuras_extras} se
encuentran figuras mostrando el mismo gráfico cuando se cambia el material de
las líneas conductoras. La figura \ref{fig:resp_pedot_alum} muestra el caso del
aluminio, la figura \ref{fig:resp_pedot_est} muestra la respuesta con estaño y
la figura \ref{fig:resp_pedot_oro} muestra que ocurre cuando se utiliza oro. Al
igual que con el caso del PPy, no existen muchas diferencias al cambiar el
material de las líneas conductoras.}
\label{fig:resp_pedot}
\end{figure}

\begin{figure}
\centering
% \includegraphics[width=\textwidth]{images/chamber_PANI_in_air.png}
\input{images/chamber_PANI_in_air-Cobre.pgf}
\caption[Impedancia del sensor con PANI en aire]{Respuesta del sistema con una
película de PANI cuando la cámara de gas está llena de aire. El eje vertical
representa la impedancia y está en escala logarítmica, mientras que el eje $x$
representa la frecuencia. La impedancia del PANI es la que más varía con
respecto a la frecuencia cuando se compara con las otras dos películas de
polímero. En este caso se utiliza, al igual que en las figuras anteriores, cobre
como material conductor de la \textit{slot line}. En el apéndice
\ref{chap:figuras_extras} se encuentran las gráficas para este caso cuando se
cambia el material de los conductores, estas con las figuras
\ref{fig:resp_pani_alum}, \ref{fig:resp_pani_est} y \ref{fig:resp_pani_oro} para
el aluminio, el estaño y el oro, respectivamente. Aquí, nuevamente, se ve que
los cambios en los conductores no afectan en gran medida.}
\label{fig:resp_pani}
\end{figure}

Si se comparan las figuras \ref{fig:resp_ppy}, \ref{fig:resp_pedot} y
\ref{fig:resp_pani} con sus equivalentes encontradas en el apéndice
\ref{chap:figuras_extras} (estas son las figuras \ref{fig:resp_ppy_alum},
\ref{fig:resp_ppy_est} y \ref{fig:resp_ppy_oro} para el PPy;
\ref{fig:resp_pedot_alum}, \ref{fig:resp_pedot_est} y \ref{fig:resp_pedot_oro}
para el PEDOT y \ref{fig:resp_pani_alum}, \ref{fig:resp_pani_est} y
\ref{fig:resp_pani_oro} para el PANI), nuevamente, se observa que cambiar el
material de los conductores no afecta de gran manera la respuesta, mientras que
cambiar el material del substrato sí lo hace. Los cambios en el material de la
película, por otro lado, generan cambios considerables en la impedancia del
sistema y en el comportamiento de las curvas.  El comportamiento del PANI es el
que más varía en el rango de frecuencia, los otros polímeros, con mayor
conductividad, no presentan variaciones tan bruscas de su impedancia, pero sí
tienen un comportamiento menos predecible en baja frecuencia.  La figura
\ref{fig:poly_comp} muestra una comparación entre las impedancias del sistema al
utilizar diferentes películas de polímero, el sensor en cuestión se simuló con
cobre como material conductor y FP4 como material del sustrato. En el gráfico se
presentan diferentes escalas para cada polímero dado que sus variaciones se
presentan en diferentes escalas, sobre todo con respecto al PANI, por lo cual
comparar todas las curvas con el mismo eje vertical resultaría en un gráfico en
donde no se pueden apreciar las diferencias entre las curvas.

% Debería poner algo más acá, pero no sé qué
% Aquí debería ir las comparativas de las impedancias con y sin polímeros

\begin{figure}
\centering
% \includegraphics[width=0.8\textwidth]{images/comparacion_impedancias_chamber.png}
% \includesvg[width=\textwidth]{images/comparacion_impedancias_chamber.svg}
\input{images/comparacion_impedancias_chamber.pgf}
\caption[Comparativa de impedancias]{Comparativa entre las impedancias del
sensor con diferentes polímeros, utilizando una guía de ondas fabricada con
cobre y FP4 como material de los conductores y sustrato, respectivamente. Para
poder apreciar todas las variaciones a la vez se eligió presentar tres escalas
diferentes en el eje $y$, todas representan la impedancia, pero cada una
corresponde a una película diferente. El eje vertical que se encuentra del todo
a la izquierda en rojo, representa la impedancia del PANI, el eje vertical que
se encuentra a la derecha en azul representa la impedancia del PEDOT:PSS y aquel
de más a la derecha en verde representa la impedancia del PPy. Para los tres
casos el eje $x$ representa la frecuencia.}
\label{fig:poly_comp}
\end{figure}

Si se toma la diferencia entre el sistema sin polímero y el sistema con polímero
se puede estimar el valor de la impedancia del polímero solo. En la figura
\ref{fig:comp_pani} se muestra una comparativa entre la impedancia de la guía de
ondas sin el polímero con respecto a esta misma cuando se agrega una película de
PANI, además se gráfica la diferencia entre estas dos impedancias, lo cual
representa la impedancia del polímero. Se ve que la diferencia comienza a
disminuir a medida que aumenta la frecuencia dado que la impedancia del sensor
con polímero cae más rápido que la impedancia de la guía de ondas sola. En el
apéndice \ref{chap:figuras_extras} se agregan este tipo de gráficas para la
misma configuración con otros polímeros, particularmente las figuras
\ref{fig:comp_ppy} y \ref{fig:comp_pedot} corresponden a este mismo concepto de
gráfico para el sensor con PPy y PEDOT, respectivamente. No obstante, para estos
últimos polímeros las curvas que representan el sistema sin polímero y la
diferencia no son tan distinguibles dado que la impedancia del sistema con
polímero no está dentro del mismo orden de magnitudes que aquella de la guía de
ondas sola.

\begin{figure}
\centering
% \includesvg[width=0.8\textwidth]{images/comparativa_impedancias_pani.svg}
\input{images/comparativa_impedancias_pani.pgf}
\caption[Comparativa de impedancias para un sensor con PANI]{Comparativa de
impedancias del sensor sin polímero, el sensor con PANI y la diferencia entre
ambas impedancias. En el eje horizontal se encuentra la frecuencia para todas
las curvas. El eje vertical de la izquierda, en azul, presenta la impedancia del
sensor con el polímero, mientras que el de la derecha, en negro, presenta las curvas en
rojo y verde, las cuales corresponden al sensor sin polímero y la diferencia
entre éste y el sensor con polímero, respectivamente, en escala logarítmica. En
el apéndice \ref{chap:figuras_extras} se encuentran las correspondientes
imágenes comparando esta diferencia para los otros dos polímeros,
\ref{fig:comp_ppy} muestra esta gráfica para el PPy y \ref{fig:comp_pedot} la
muestra para el PEDOT.}
\label{fig:comp_pani}
\end{figure}

En general, se puede decir que el comportamiento del sensor postulado no
cambiará de manera drástica al cambiar el material de los conductores, pero la
elección del sustrato y, sobre todo, del polímero afectarán a su comportamiento
cuando se habla del estudio de frecuencia e impedancias en aire. Estos
parámetros podrían ser estudiados meticulosamente para elegir las mejores
combinaciones que proporcionen mejores respuestas en trabajos ulteriores.
