% Lineas de transmisión:
% - Conceptos básicos
% - Modos de transmisión
% - Vínculo entre líneas y guías de onda

\subsection{Lineas de transmisión}
\label{sub_sec:lineas_de_transmisión}

Las líneas de transmisión hacen referencia a cualquier medio o estructura que
sirva para transmitir energía, desde el sistema nervioso hasta la fibra óptica.
En esta sección se detallará la teoría de aquellas relacionadas con la
transmisión de ondas electromagnéticas con la finalidad de comprender mejor la
siguiente sección sobre un caso particular de este área de estudio.

En general, una línea de transmisión es una red de dos puertos en donde un
puerto se entiende como el transmisor y el otro como el receptor. El puerto
transmisor se considera aquel puerto desde el cual es emitida una señal. La
teoría de circuitos nos dice que la fuente de una señal puede ser representada
con su equivalente de Thevenin, el cual descompone dicha fuente emisora en un
generador más una resistencia en serie equivalente.

El extremo receptor de la línea de transmisión se conoce como carga, el cual se
representa por un resistencia o impedancia de carga ($Z_L$). La figura
\ref{fig:linea_de_transmision} representa una línea de transmisión en la cual
los puertos $A$ y $A'$ son los puertos de entrada a la línea mientras que $B$ y
$B'$ son los puertos de salida. $Z_G$ y $Z_L$ son las impedancias de entrada y
carga respectivamente y $V_G$ es la fuente generadora de Thevenin.

\begin{figure}[h]
\centering
\begin{circuitikz}[american voltages]
\draw
(0,0) to[V, V=$V_G$, invert] (0,3)
to[R=$Z_G$] (3,3)
;
\draw (3,3) node[ocirc] (A) {$A$};
\draw (3,0) node[ocirc] (Ap) {$A'$};
\draw (0,0) to[short] (Ap);
\draw (9,3) node[ocirc] (B) {$B$};
\draw (9,0) node[ocirc] (Bp) {$B'$};
\draw (B) to[short] (11, 3)
to[R=$Z_L$] (11, 0);
\draw (Bp) to[short] (11, 0);
\draw[thick] (3.5,3.5) rectangle (8.5,-0.5);
\draw (6, 1.5) node[] {Línea de transmisión};
\draw (A) to[short] (3.5, 3);
\draw (Ap) to[short] (3.5, 0);
\draw (B) to[short] (8.5, 3);
\draw (Bp) to[short] (8.5, 0);
\end{circuitikz}
\caption{Representación de una línea de transmisión.}
\label{fig:linea_de_transmision}
\end{figure}

\subsubsection{Modos de transmisión}
\label{subsub_sec:modos_de_transmisión}

Las ondas electromagnéticas pueden propagarse por la línea de transmisión de
varias maneras. Por lo cual, dependiendo de la forma en que se propagan las
ondas por el medio, las líneas de transmisión se clasifican en:
\begin{enumerate*}[label={\arabic*)}]
\item líneas transversales electromagnéticas y
\item líneas de transmisión de orden superior.
\end{enumerate*}
Estas describen la forma en la que los campos eléctricos y magnéticos se
comportan con respecto a la dirección de propagación

En las líneas de transmisión transversal electromagnéticas o \acrshort{tem}
(\textit{transverse electromagnetic})  los campos eléctricos y magnéticos se
propagan totalmente transversales a la dirección de
propagación. Mientras que en las líneas de transmisión de orden superior las
ondas tienen al menos un componente que se propaga en la dirección de
propagación.

\subsubsection{Modelo de parámetros distribuidos}
\label{subsub_sec:modelo_de_parámetros_concentrados}
Una línea de transmisión TEM se puede representar por un circuito equivalente.
Si se analiza cualquier línea de transmisión de este estilo, se puede notar que
están compuestas por dos conductores en paralelo  separadas por algún medio. La
teoría electromagnética nos indica a simple inspección que este componente
presenta una capacitancia asociada. De la misma manera los conductores que
puedan componer la línea, al no ser conductores perfectos, presentan alguna
resistencia equivalente. Al pasar corriente por dichos conductores se crea una
inductancia asociada. Y, de la misma manera que los conductores presentan alguna
resistencia, el medio aislante puede presentar alguna conductancia. Por lo tanto
una trozo de línea de longitud $\Delta z$ tiene una resistencia $R$ asociada por
unidad de longitud medida en $\si{\ohm/\metre}$, una inductancia $L$ medida en
$\si{\henry/\metre}$, una conductancia $G$ medida en $\si{\siemens/\metre}$ y
una capacitancia $C$ medida en $\si{\farad/\metre}$ y puede ser representada por
el modelo de circuito equivalente de la figura
\ref{fig:linea_de_transmision_equivalente}.

\begin{figure}[h!]
\centering
\begin{circuitikz}[american voltages]
\draw (0, 0) node[ocirc] (A) {} node[above] {$-$};
\draw (0, 3) node[ocirc] (B) {} node[below] {$+$};
\draw[-latex] (0, 2.5) -- node[below] {$i(z{,} t)$} (2, 2.5);
\draw (B) to [R=$R \Delta z$] (3, 3)
to [L=$L\Delta z$] (6, 3)
to [R=$G \Delta z$] (6, 0)
to [short] (A)
to [open=$v(z{,} t)$] (B);
\draw (6, 3) to[short] (9, 3)
to [C=$C \Delta z$] (9, 0)
to [short] (6, 0);
\draw (11, 3) node[ocirc] (C) {} node[below] {$+$};
\draw (11, 0) node[ocirc] (D) {} node[above] {$-$};
\draw[-latex] (9.5, 2.5) -- node[below] {$i(z + \Delta z{,} t)$} (11, 2.5);
\draw (9, 3) to [short] (C);
\draw (9, 0) to [short] (D);
\draw (C) to [open=$v(z + \Delta z {,} t)$] (D);
\draw[latex-latex, thick] (0, -0.5) -- node[below] {$\Delta z$} (11, -0.5);
\end{circuitikz}
\caption{Circuito equivalente de una línea de transmisión}
\label{fig:linea_de_transmision_equivalente}
\end{figure}

\subsubsection{Ecuaciones de telégrafo}
\label{subsub_sec:ecuaciones_de_telégrafo}
En el circuito de la figura \ref{fig:linea_de_transmision_equivalente} se puede
aplicar la ley de voltajes de Kirchhoff y obtener:

\begin{equation}
v(z, t) - R\Delta z i(z,t) - L \Delta z \frac{\partial i(z, t)}{\partial t}
- v(z + \Delta z, t) = 0
\end{equation}

Mientas que aplicando la ley de corrientes se obtiene:

\begin{equation}
i(z, t) - G \Delta z v(z + \Delta z, t)
- C \Delta z \frac{\partial v(z + \Delta z, t)}{\partial t}
- i(z + \Delta z, t) = 0
\end{equation}

Al dividir ambas ecuaciones por $\Delta z$ y tomar el límite cuando $\Delta z
\to 0$ se obtienen dos ecuaciones diferenciales:

\begin{align}
\frac{\partial v(z, t)}{\partial z} &= -R i(z, t)
- L \frac{\partial i(z, t)}{\partial t} \label{eq:parcial_voltaje} \\
\frac{\partial i(z, t)}{\partial z} &= -G v(z, t)
- C \frac{\partial v(z, t)}{\partial t} \label{eq:parcial_corriente}
\end{align}

Este par de ecuaciones son conocidas como las ecuaciones del telégrafo y pueden
representar las características típicas de las líneas de transmisión TEM. Para
las condiciones de estado estacionario, con fasores cosenoidales, las ecuaciones
se reducen a \cite{pozar2011microwave}:

\begin{align}
	\frac{\mathrm{d}V(z)}{\mathrm{d}z} &= -(R + \jmath \omega L) I(z)
	\label{eq:tel_voltaje} \\
	\frac{\mathrm{d}I(z)}{\mathrm{d}z} &= -(G + \jmath \omega C) V(z)
	\label{eq:tel_corriente}
\end{align}


% End of líneas_de_transmisión
