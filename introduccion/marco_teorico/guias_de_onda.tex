% Guías de onda:
% - Coplanares
% - Slotline
% - Ecuaciones de líneas de transmisión
\subsection{Guías de ondas}
\label{sub_sec:guías_de_ondas}

Las guías de onda son utilizadas para transmitir energía electromagnética de un
punto a otro de manera eficiente. Un ejemplo típico de estas sería el cable
coaxial. Las guías de onda son sistemas de dos puertos y, por consiguiente, se
pueden interpretar como líneas de transmisión. Por lo cual la misma teoría de
la sección anterior (\ref{sub_sec:lineas_de_transmisión}) se puede aplicar en
este concepto.

Previamente se explicó como, dependiendo de la manera en que se propaga la onda
electromagnética por el medio, se podían clasificar los modos de transmisión en
modos TEM y modos de orden superior (para aquellas que no soportan el modo TEM).
Para comprender mejor esto, es ideal poner esto en términos matemáticos.
Partiendo por la idea de  que las ecuaciones de propagación se pueden
descomponer por sus componentes transversales y longitudinales, se toma como
sistema de coordenadas un sistema cartesiano y se acuerda que la dirección de
propagación es en el eje $z$, se pueden entender las ecuaciones de onda como:

\begin{align}
\vec{E}(x, y, z, t) &= \vec{E}(x, y) e^{\jmath \omega t - \jmath \beta z} \\
\vec{H}(x, y, z, t) &= \vec{H}(x, y) e^{\jmath \omega t - \jmath \beta z}
\end{align}

En donde $\omega$ es la frecuencia angular y $\beta$ es el número de onda de
propagación en la dirección de $z$. Las ondas pueden entonces descomponerse
como:

\begin{equation}
	\vec{E}(x, y) = \underbrace{\hat{x} E_x(x, y)
	+ \hat{y} E_y(x, y)}_\text{transversal}
	+ \underbrace{\hat{z} E_z(x, y)}_\text{longitudinal}
	\equiv \vec{E}_T (x, y) + \hat{z}E_z (x, y)
\end{equation}

De la misma manera se puede descomponer el operador gradiente

\begin{equation}
	\nabla =
	\underbrace{\hat{x} \partial_x + \hat{y} \partial_y}_\text{transversal}
	+ \hat{z} \partial_z
	= \nabla_T + \hat{z} \partial_z
	= \nabla_T - \jmath \beta \hat{z}
\end{equation}

El reemplazo de $\partial_z \to - \jmath \beta \hat{z}$ se hace asumiendo la
dependencia de $z$. Al introducir estas descomposiciones en las ecuaciones de
Maxwell libres de fuentes:

\begin{align}
	\nabla \times \vec{E} &= - \jmath \omega \mu \vec{H} \\
	\nabla \times \vec{H} &= \jmath \omega \epsilon \vec{E} \\
	\nabla \cdot \vec{E} &= 0 \\
	\nabla \cdot \vec{H} &= 0
\end{align}
estas se transforman en:

\begin{align}
	(\nabla_T - \jmath \beta \hat{z}) \times (\vec{E}_T + \hat{z}E_z)
	&= - \jmath \omega \mu (\vec{H}_T + \hat{z}H_z) \\
	(\nabla_T - \jmath \beta \hat{z}) \times (\vec{H}_T + \hat{z}H_z)
	&= - \jmath \omega \epsilon (\vec{E}_T + \hat{z}E_z) \\
	(\nabla_T - \jmath \beta \hat{z}) \cdot (\vec{E}_T + \hat{z}E_z)
	&= 0 \\
	(\nabla_T - \jmath \beta \hat{z}) \cdot (\vec{H}_T + \hat{z}H_z)
	&= 0
\end{align}
en donde $\mu$ y $\epsilon$ denotan la permeabilidad magnética y la
permitividad eléctrica,
respectivamente de los medios.

Usando las propiedades del algebra líneal $\hat{z} \cdot \hat{z} = 1$, $\hat{z}
\times \hat{z} = 0$, $\hat{z} \cdot \vec{E}_T = 0$, $\hat{z} \cdot \nabla_T
E_z = 0$ y que:

\begin{gather*}
	\hat{z} \times \vec{E}_T = \hat{z} \times
	(\hat{x}E_x + \hat{y} E_y)
	= \hat{y}E_x - \hat{x} E_y \\
	\nabla_T \times \vec{E}_T = (\hat{x} \partial_x + \hat{y} \partial_y)
	\times (\hat{x} E_x + \hat{y}E_y)
	= \hat{z} (\partial_x E_y - \partial_y E_x)
\end{gather*}
se obtiene un set de ecuaciones de Maxwell:

\begin{align}
	\nabla_T E_z \times \hat{z} - \jmath \beta \hat{z} \times \vec{E}_T
	&= - \jmath \omega \mu \vec{H}_T \label{eq:max_prim} \\
	\nabla_T H_z \times \hat{z} - \jmath \beta \hat{z} \times \vec{H}_T
	&= \jmath \omega \epsilon \vec{E}_T \label{eq:max_sec} \\
	\nabla_T \times \vec{E}_T + \jmath \omega \mu \hat{z} H_z
	&= 0 \\
	\nabla_T \times \vec{H}_T - \jmath \omega \epsilon \hat{z} E_z
	&= 0 \\
	\nabla_T \cdot \vec{E}_T - \jmath \beta E_z
	&= 0 \\
	\nabla_T \cdot \vec{H}_T - \jmath \beta H_z
	&= 0 \\
\end{align}

Luego, dependiendo si algunas de las componentes longitudinales son cero, se
pueden clasificar los modos de propagación en modos: TEEM $E_z = H_z = 0$, TE
(Transversal Eléctrico) $E_z = 0$, $H_z \ne 0$, TM (Transversal Magnético) $E_z
\ne 0$, $H_z = 0$ y modo híbrido $E_z \ne 0$, $H_z \ne 0$.

Haciendo un producto cruz con $\hat{z}$ en la ecuación \ref{eq:max_sec} y
utilizando las identidades: $\hat{z} \times (\hat{z} \times \vec{H}_T) =
\hat{z}(\hat{z} \times \vec{H}_T) - \vec{H}_T(\hat{z} \cdot \hat{z}) =
-\vec{H}_T$, y de manera similar, $\hat{z} \times (\nabla_T H_z \times \hat{z})
= \nabla_T H_z$, se obtiene:

\begin{equation}\label{eq:max_sec_reorder}
	\nabla_T H_z + \jmath \beta \vec{H}_T
	= \jmath \omega \epsilon \hat{z} \times \vec{E}_T
\end{equation}

Por lo tanto utilizando las ecuaciones \ref{eq:max_prim} y
\ref{eq:max_sec_reorder}, y dividiendo por $\jmath$ se puede obtener un sistema
de ecuaciones con $\hat{z} \times \vec{E}$ y $\vec{H}_T$ como incógnitas. Una
posible solución a este sistema es \cite{orfanidis2002electromagnetic}:

\begin{align}
	\hat{z} \times \vec{E}_T &=
	- \frac{\jmath \beta}{k^2_c} \hat{z} \times \nabla_T E_z
	- \frac{\jmath \omega \mu}{k^2_c} \nabla_T H_z \\
	\vec{H}_T &=
	- \frac{\jmath \omega \epsilon}{k^2_c} \hat{z} \times \nabla_T E_z
	- \frac{\jmath \beta}{k^2_c} \nabla_T H_z
\end{align}

Donde $k_c$ es el número de corte de onda y esta definido por:

\begin{equation}
k^2_c = \omega^2 \epsilon_0 \mu_0 - \beta^2 = \frac{\omega^2}{c_0^2} - \beta^2
= k^2 - \beta^2
\end{equation}
donde $c_0$ es la velocidad de la luz en el vacío, al igual que $\epsilon_0$ y
$\mu_0$ son la permitividad eléctrica y la permeabilidad magnética,
respectivamente, en el vacío. Algunos otros valores
relacionados son la frecuencia de corte y la longitud de onda de corte:

\begin{equation}
\omega_c = c_0 k_c, \qquad \lambda_c = \frac{2 \pi}{k_c}
\end{equation}

Estas variables $\mu$, $\epsilon$, $\omega_c$ y $k_c$; caracterizan a cualquier
guia de ondas.

\subsection{Slot line}
\label{sub_sec:slot_line}

Las \textit{slot line} son guías unipolares en
donde todos los conductores están del mismo lado del substrato
\cite{verma2021introduction}. Una \textit{slot
line} consiste de una ranura en una capa conductora depositada sobre un
substrato como se ve en la figura \ref{fig:slot_line}. Estas fueron introducidas
por primera vez en 1969 por Seymour Cohn \cite{cohn1969slot}, en su trabajo hace
notar que la geometría de la guia la hace conveniente para conectar dispositivos
entre los conductores.

\begin{figure}[h]
\centering
% \includegraphics[scale=.5]{images/slot_line_completa.png}
\def\svgwidth{0.5\columnwidth}
\input{images/slot_line_completa.pdf_tex}
\caption{Representación gráfica de una \textit{slot line}.}
\label{fig:slot_line}
\end{figure}

Para caracterizar una \textit{slot line}, y cualquier otra guía de ondas, es
necesario conocer el modelo de parámetros distribuidos de esta identificando su
impedancia característica, la cual queda dada por la impedancia equivalente del
circuito de la figura \ref{fig:linea_de_transmision_equivalente}. Gevorgian y
Berg \cite{gevorgian2001line} demostraron que la capacitancia de una
guía de ondas coplanar con líneas conductoras de ancho $s$, ancho de ranura de
$2g$, altura del substrato $h$ (como la mostrada en la figura
\ref{fig:slot_line}) está dada por:
\begin{equation}\label{eq:slot_cap}
	C = \epsilon_o \epsilon_{\mathrm{eff}_1} \frac{K(k'_o)}{K(k_o)}
\end{equation}
donde la constante dieléctrica efectiva está
definida como $\epsilon_{\mathrm{eff}_1} = 1 + (\epsilon - 1)q$, con:

\begin{equation}
q = \frac{1}{2} \frac{K(k')}{K(k)} \frac{K(k_o)}{K(k'_o)}
\end{equation}
donde:
\begin{align}
	k &= \frac{\tanh \left( \frac{\pi g}{2h}\right)}
	{\tanh \left( \frac{\pi (s + g)}{2h}  \right) } \\
	k' &= \sqrt{(1 - k^2)} \\
	k_o &= \frac{g}{s + g} \\
	k'_o &= \sqrt{1 - k_o^2}
\end{align}
y $K(*)$ es la integral elíptica de primer orden de $*$ dada por
\cite{encyclopediaofmath}:
\begin{equation}
	K(h) = \int_{0}^{\phi} \frac{\mathrm{d}t}{\sqrt{1 - h^2 \sin^2t}}
\end{equation}
esta puede ser calculada utilizando el la librería de Scipy de Python
\cite{ellipk}.


Más adelante se desarrollará un modelo completo que caracteriza la guía de
ondas utilizando esta ecuación.
