% Sensores:
% - Conceptos generales sobre sensores
% - Sensibilidad
% - Selectividad
% - Tiempo de respuesta
\subsection{Sensores}
\label{sub_sec:sensores}

Jacob Franden, en su libro \textit{Handbook of modern sensors: Physics, designs
and applications}, define que un sensor es un dispositivo que recibe un estímulo
y responde con una señal eléctrica \cite{jacob2003handbook}. Este mismo autor
describen como se
comportan los sensores utilizados en la modernidad. El término estímulo hace
referencia a una cantidad o propiedad que es recibida (por el sensor). Un sensor,
por lo tanto, ``transforma'' una señal (estímulo), ya sea una reacción química o la
intensidad de luz o la resistividad de un material, en otra que puede ser medida
por un circuito electrónico. En el caso del estudio presentado en este
documento, se espera detectar una reacción química y relacionarla con una
variable eléctrica, de modo que el procesamiento de dicha variable nos exponga
una estimación de la concentración del gas que produjo la reacción.

Se puede entender un sensor, matemáticamente, con una función de transferencia.
Para poder determinar el comportamiento de un sensor, se deben entender cuales
son las características fundamentales que representan su comportamiento, tales
características son la sensibilidad, la precisión, la resolución entre otras.
Esta sección se encarga de definirlas.

\subsubsection{Sensibilidad}
\label{subsub_sec:sensibilidad}
Si se piensa de un sensor como una función matemática que ``convierte'' un valor
en otro, la expresión más simple para representarlo seria una ecuación lineal
afín o ecuación de la recta:

\begin{equation}
E = A + Bs
\end{equation}
en donde $s$ es la variable independiente, en este caso el estímulo, y $E$ es el
valor resultante de la función, la señal eléctrica. $A$ es el valor donde la
señal intercepta a la función de salida $E$ en cero. $B$ es la pendiente de la
recta y se puede entender de esta como la \textbf{sensibilidad} dado que
representa la proporción en que el estímulo afecta al valor de salida $E$. No
obstante, la relación entre es estímulo y la señal de salida no siempre es
lineal, de hecho, en la mayoría de los casos el comportamiento solo se acerca a
la linealidad.

Dado que la sensibilidad es la pendiente de la curva de la función de
transferencia que representa al estímulo, se puede entender que, para los casos
no lineales, la sensibilidad es la derivada de la función para un estímulo
particular.

\begin{equation}
	b_i(s_i) = \frac{\mathrm{d}E(s_i)}{\mathrm{d}s}
	= \frac{\Delta E_i}{\Delta s_i}
\end{equation}
donde $\Delta s_i$ es un pequeño cambio en el estímulo de entrada y $\Delta E_i$
es un variación en la señal de salida.

\subsubsection{Precisión}
\label{subsub_sec:precisión}
Al medir cualquier cantidad existe cierta imprecisión, esto dado a que en la
realidad nunca se puede saber con exactitud el valor real de una muestra. La
desviación del valor real se puede comprobar en pruebas controladas. Todos los
sensores tienen alguna desviación con respecto al valor real. Por ejemplo, en
un sensor para medir, se toma una medida de $10 (\si{\milli\metre})$ y se espera
que esta se corresponda en una relación $1:1$ con un voltaje de salida del
sensor, por lo tanto $B = 1 (\si{\milli\volt/\milli\metre})$, no obstante, al
comprobar el resultado $E = 10.5 (\si{\milli\volt})$. Esto nos muestra una
desviación de $0.5 (\si{\milli\metre}) / 10 (\si{\milli\metre})$, un error del
$5\%$. Para un instrumento real se deberían hacer varias pruebas hasta encontrar
una estadística del error de medición con esto, se podría decir que el sensor
presenta un error sistemático en el rango de mediciones controladas. La
precisión del sensor estará dada por este error, con esto se espera que el valor
real del lo que se espera medir caiga en el rango del valor medido $\pm$ el
error, esto es denotado como $x \pm \Delta$ donde $\Delta$ es el error del
instrumento. En la práctica se esperaría que el valor real se encuentre dentro
del rango de $x \pm \delta$ donde $\Delta \ge \delta$. Para esto, $\Delta$
siempre se estima en el peor de los casos.

\subsubsection{Saturación}
\label{subsub_sec:saturación}
Todo sensor tiene un rango límite de operación (y cualquier componente
electrónico presenta el mismo comportamiento). Cuando el estímulo supera el
rango límite, la respuesta de salida ya no tendrá ninguna relación con la
entrada. Por lo tanto se considera ese punto como el punto de saturación y se
dice que el sensor se ha saturado o entrado en saturación.

\subsubsection{Resolución}
\label{subsub_sec:resolución}
La resolución se refiere al valor mínimo que un sensor es capaz de detectar,
vale decir, el incremento más pequeño del estímulo que es posible detectar. En
el caso de un instrumento digital, la resolución está dada por el número de
bits. Por ejemplo: se tiene una regla de $30 \si{\centi\metre}$, en dicha regla
existen demarcaciones enumeradas cada centímetro y marcas sin enumerar cada un
milímetro; el valor mínimo que se puede medir con una regla como esta será de
$1 \si{\milli\metre}$ y cualquier medición realizada no podrá tener una
resolución menor a un milímetro.

\subsubsection{Repetibilidad}
\label{subsub_sec:repetibilidad}
El error de repetibilidad es el error causado por la incapacidad del sensor de
producir el mismo resultado con la misma entrada.

\subsubsection{Otros parámetros}
\label{subsub_sec:otros_parámetros}
En sensores de gas existen algunos parámetros extra que son aplicables a este
tipo de mediciones en particular y no son tan frecuentemente utilizadas en otros
campos.

\begin{itemize}
\item \textbf{Selectividad:} Hace referencia a si el sensor es capaz de
	diferenciar el gas partícular que necesita medir en una muestra con
	variados gases presentes.
\item \textbf{Límite de detección:} Hace referencia a la concentración mínima de
	analito que el sensor es capaz de detectar bajo ciertas condiciones.
\item \textbf{Tiempo de respuesta:}  Es el tiempo que le toma al sensor
	responder a un estímulo de concentración.
\item \textbf{Tiempo de recuperación:} Es el tiempo que le toma al sistema en
	volver a sus valores iniciales luego de haber sido expuesto a una
	muestra.
\end{itemize}
