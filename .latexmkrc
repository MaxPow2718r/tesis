@default_files = ('main.tex');

# name of the output files, shall not contain spaces
$jobname = 'Tesis-Pablo_Velasquez';

# add_cus_dep('glo', 'gls', 0, 'makeglo2gls');
# sub makeglo2gls {
#     system("makeindex -s '$_[0]'.ist -t '$_[0]'.glg -o '$_[0]'.gls '$_[0]'.glo");
# }

# biber configuration
$biber = 'biber %O %S';
$bibtex_use = 1;

# set the clean up mode to clean everything but pdf, dvi or ps files
$cleanup_mode = 2;
$clean_ext = 'bbl run.xml synctex.gz';

$pdf_previewer = 'zathura';

# pdflatex options
$pdf_mode = 1;
$pdflatex = 'pdflatex -synctex=1 --interaction=nonstopmode -shell-escape %O %S';
